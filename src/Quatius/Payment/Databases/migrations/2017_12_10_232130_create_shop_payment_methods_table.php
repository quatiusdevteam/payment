<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Quatius\Payment\Models\PaymentMethod;

class CreateShopPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150);
            $table->string('type', 50);
            $table->enum('mode',['env','sandbox','live'])->default('sandbox');
            $table->text('live_credentials')->nullable();
            $table->text('sandbox_credentials')->nullable();
            $table->text('params')->nullable();
            $table->text('notes')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('ordering')->unsigned()->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_payment_methods');
    }
}
