<?php

namespace Quatius\Payment\Events;

class PaymentListeners {
    
    //lookup reference only
    public static $event_list = [
        'shop.payment-failed'=>['order', 'response'],
        'shop.payment-pending'=>['order', 'response'],
        'shop.payment-checkout-confirmed'=>['order', 'response'],
        'shop.payment-confirmed'=>['order', 'response']
    ];

    public function subscribe($events)
    {

        $events->listen('shop.payment-pending', function($order, $response){
            // $order->updateStatus('pending');
            
            // $payment = $order->getPaymentMethod();
            // $order->updateHistoryLinkStatus($payment, [
            //     'status'=>'pending',
            //     'comments'=>'Pending payment from '.$payment->name,
            //     'params'=>($response)?json_encode($response->getData()):'[]'
            // ]);
            
        });
        
        $events->listen('shop.payment-notify', function($order, $status, $amt, $tranId, $data){
            // if ($status == "completed" || $status == "confirmed"){
            //     if ($order->status != "confirmed"){
            //        $order->updateStatus('confirmed');
            //     }
            //     $order->setParams('payment.reciept_no', $tranId);
            //     $order->save();

            //     $status = ($status=="completed"?"accepted":"confirmed");

            //     $payment = $order->getPaymentMethod();
            //     $order->updateHistoryLinkStatus($payment, [
            //         'status'=>$status,
            //         'value'=>$tranId,
            //         'comments'=>"Payment $status from ".$payment->name,
            //         'params'=>json_encode($data)
            //     ]);
            // }
            // elseif ($status == "refunded" || $status =="partially refunded"){
            //     $order->updateStatus('refunded');
            //     $order->save();
                
            //     $payment = $order->getPaymentMethod();
            //     $order->updateHistoryLinkStatus($payment, [
            //         'status'=>'refunded',
            //         'value'=>$tranId,
            //         'comments'=>"Payment refunded of ".format_currency($amt),
            //         'params'=>json_encode($data)
            //     ]);
            // }
            // elseif ($status == "checkout"){
                
            //     $payment = $order->getPaymentMethod();
            //     $order->updateHistoryLinkStatus($payment, [
            //         'status'=>'checkout',
            //         'value'=>$tranId,
            //         'comments'=>'Request checkout info from '.$payment->name,
            //         'params'=>json_encode($data)
            //     ]);
            // }
            // else{
            //     $payment = $order->getPaymentMethod();
            //     $order->updateHistoryLinkStatus($payment, [
            //         'status'=>'unknown',
            //         'value'=>$tranId,
            //         'comments'=>"Unknown status('$status') from ".$payment->name,
            //         'params'=>json_encode($data)
            //     ]);
            // }
        });
        
        // $events->listen('shop.payment-confirmed', function($order, $response){
        //     Event::fire('shop.payment-notify',[$order, 'confirmed', $order->total, $response->getTransactionReference(), $response->getData()]);
        // });
        
        // $events->listen('shop.payment-checkout-confirmed', function($order, $response){
        //     Event::fire('shop.payment-notify',[$order, 'checkout', $order->total, "", $response->getData()]);
        // });
    }
}