<?php

namespace Quatius\Payment\Models;


interface IPaymentOrder
{	
    function getPaymentMethod();
    function getPaymentItems();
    function getPaymentTotal();
    function getPaymentShipTotal();
    function getPaymentCurrency(); // AUD
    function getPaymentDescription();
    function getPaymentInvoiceNo();
}