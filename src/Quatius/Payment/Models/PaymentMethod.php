<?php

namespace Quatius\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Quatius\Framework\Traits\PublishStatus;
use View;
use Quatius\Framework\Traits\ModelFinder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Quatius\Framework\Traits\ParamSetter;

class PaymentMethod extends Model
{	
    use ParamSetter, PublishStatus, ModelFinder, SoftDeletes;
    
    protected $table = 'shop_payment_methods';
	
	protected $fillable = [
	    'name',
	    'type',
	    'mode',
	    'live_credentials',
	    'sandbox_credentials',
	    'params',
	    'notes',
	    'status',
	    'ordering',
	];
	
	protected $hidden = [
	    'live_credentials',
	    'sandbox_credentials'
    ];
	
	public static function boot()
    {
        parent::boot();
		
		static::creating(function ($item) {
			if (!isset($item->ordering)){
				$item->ordering =  PaymentMethod::max('ordering') + 1;
			}
		});
	}
	
	function getModeAttribute(){
		if (config('quatius.payment.payment.debug', false)) return 'sandbox';

	    if ($this->attributes['mode']=='env'){
	        return config('quatius.payment.'.$this->type.'.mode', 'sandbox');
	    }
	    return $this->attributes['mode'];
	}
	
	public function getConfig($field='', $default=null){
	    if ($this->hasParams($field)){
	        return $this->getParams($field, $default);
	    }
	    
	    return config('quatius.payment.'.$this->type.'.'.$field, $default);
	}
	/* 
	function getParamsAttribute(){
	    if ($this->attributes['mode']=='env'){
	        return config('quatius.payment.'.$this->type.'.params', []);
	    }
	    return isset($this->attributes['params'])&&$this->attributes['params']!=null?json_decode($this->attributes['params'], true):[];
	} */
	
	function getCredentialsAttribute(){
	    $mode = $this->mode;
	    
	    $credentials = [];
	    if ($this->attributes['mode']=='env'){
	        $mode = config('quatius.payment.'.$this->type.'.mode', 'sandbox');
	        $credentials = json_encode(config('quatius.payment.'.$this->type.'.'.$mode.'_credentials', []));
	    }
	    else{
	        $modeCred = $mode."_credentials";
	        $credentials = $this->$modeCred;
	    }
	    return json_decode($credentials, true);
	}
	
	public function getLogo(){
	    $logo = config('quatius.payment.'.$this->type.'.confirm.logo','');
	    
	    return $logo!=''?$logo:null;
	}
	
	public function getOrderForm(){
	    if (count($this->getValidation()) > 0){
			return $this->getView('input-form');
	    }
	    else{
			return $this->getView('order');
	    }
	}
	
	public function getValidation(){
	    return config('quatius.payment.'.$this->type.'.input-form.validation',[]);
	}

	public function getProccesingForm(){
		return $this->getView('processing-form');
	}

	public function getView($view = '', $useDefault = true){
		if (!$view) return null;
		
		$view_override = $this->getConfig('views.'.$view, null);
	    
	    if($view_override && View::exists($view_override)){
	        return $view_override;
		}
		
	    $view_override = "Payment::payment.types.".$this->type.".".$view;
	    
	    if(View::exists($view_override)){
	        return $view_override;
	    }
		
		if ($useDefault)
			return "Payment::payment.types.default.".$view;
			
		return null;
	}
	
	public function requiredForm(){
	    return !!config('quatius.payment.'.$this->type.'.input-form', false);
	}
	
	public function isDebugMode(){
	    return $this->mode != 'live';
	}
}
