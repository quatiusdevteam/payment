<?php

namespace Quatius\Payment\Models;

use Quatius\Payment\Traits\PerformPayment;

class StandardOrder
{	
    use PerformPayment;
    
    public function getPaymentItems(){
        return [

        ];
    }

    public function getPaymentTotal(){
        return "120.00";
    }

    public function getPaymentShipTotal(){
        return "10.00";
    }

    public function getPaymentDescription(){
        return "test INV123456";
    }

    public function getPaymentInvoiceNo(){
        return "INV123456";
    }
}