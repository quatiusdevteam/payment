<?php

namespace Quatius\Payment\Providers;

use Illuminate\Support\ServiceProvider;
use Event;
use Quatius\Payment\Events\PaymentListeners;

class ShopPaymentProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	
    public function boot()
    {
    	$this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
    	$this->publishes([
    	     realpath(__DIR__.'/../').'/Publishes/Payment'=> app_path('Modules/Payment'),
    	]);
    	
    	$this->publishes([
    	    realpath(__DIR__.'/../').'/Publishes/public'=>public_path()
    	]);
    	
    	// $this->publishes([
    	//     realpath(__DIR__.'/../').'/Publishes/public-forced'=>public_path()
    	// ], 'public');
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        Event::subscribe(PaymentListeners::class);
        
        $this->app->bind('Quatius\Payment\Repositories\PaymentRepository','Quatius\Payment\Repositories\Handlers\PaymentHandler');
        $this->app->bind('PaymentHandler','Quatius\Payment\Repositories\PaymentRepository');
    }
}
