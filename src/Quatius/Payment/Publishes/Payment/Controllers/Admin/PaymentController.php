<?php

namespace App\Modules\Payment\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use Quatius\Payment\Traits\Controllers\Admin\ManagePayment;

class PaymentController extends AdminWebController
{
    use ManagePayment;
}
