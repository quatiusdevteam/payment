<?php

namespace App\Modules\Payment\Controllers;

use Quatius\Payment\Traits\Controllers\DoPayment;
use App\Http\Controllers\Controller;

class PaymentController extends Controller {
    
    use DoPayment;
    
}
