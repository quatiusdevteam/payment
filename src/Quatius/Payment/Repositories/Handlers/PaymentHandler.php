<?php

namespace Quatius\Payment\Repositories\Handlers;

use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Payment\Repositories\PaymentRepository;
use Quatius\Framework\Traits\PublishStatus;

class PaymentHandler extends QuatiusRepository implements PaymentRepository{
    
    use PublishStatus;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return config('quatius.shop.payment.model',\Quatius\Payment\Models\PaymentMethod::class);
    }

    public function getOrder(){
        return getPaymentOrder();
    }

    public function getPublished($publishOnly=true){
        if (!$this->allowedCache('getPublished') || $this->isSkippedCache()) {
            return $this->makeModel()->status($publishOnly?2:[1,2])->get();
        }

        $key = $this->getCacheKey('getPublished', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($publishOnly) {
            return $this->makeModel()->status($publishOnly?2:[1,2])->get();
        });
            
        $this->resetModel();
        $this->resetScope();
        return $value;
    }
    // public function getByShoppers($shopperGroupIds=[]){
    //     if (!$this->allowedCache('getByShoppers') || $this->isSkippedCache()) {
    //         return $this->makeModel()->ofShoppers($shopperGroupIds)->published()->get()->keyBy('id')->values();
    //     }
        
    //     $key = $this->getCacheKey('getByShoppers', func_get_args());
    //     $minutes = $this->getCacheMinutes();
    //     $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($shopperGroupIds) {
    //         return $this->makeModel()->ofShoppers($shopperGroupIds)->published()->get()->keyBy('id')->values();
    //     });
            
    //     $this->resetModel();
    //     $this->resetScope();
    //     return $value;
    // }
}