<?php
namespace Quatius\Payment\Traits\Controllers\Admin;

use Illuminate\Http\Request;
use Omnipay\Omnipay;

trait ManagePayment{
    
    private $repo = null;
    public function getRepo()
    {
        if ($this->repo == null)
            $this->repo = app('PaymentHandler');
            
        return $this->repo;
    }
    
    public function index(Request $request){
            
        if ($request->wantsJson()) {
            $payments = $this->getRepo()->makeModel()->get();
            return response()->json(['data'=> $payments], 200);
        }

        return $this->theme->of('Payment::admin.payment.index')->render();
    }
    
    public function update(Request $request, $payment_id){
       try {

            $attributes = $request->all();
           
            $this->getRepo()->update($attributes, $payment_id);
            $payment = $this->getRepo()->findWhere(['id'=>$payment_id])->first();
            
             return response()->json([
                'message'  => 'payment is updated',
                'code'     => 204,
                'redirect' => trans_url('/admin/shop/payment/'.$payment_id.'/edit'),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/shop/payment/'.$payment_id.'/edit'),
            ], 400);

        }
    }

    public function create(Request $request){
        $gatewayNames = Omnipay::all();
        $gatewayNames = array_merge(['PayPal_Express', 'PayPal_Rest'], $gatewayNames);
        $gatewayNames = array_merge(config('quatius.payment.extra-gateways',[]), $gatewayNames);
        
        $gateways = array_map(function($name) {
            return Omnipay::create($name);
        }, $gatewayNames);

        return view('Payment::admin.payment.create',['gateways'=>$gateways]);
    }

    public function createGateway(Request $request, $gatewayName){

        $gateway = omnipay::create($gatewayName);
        return view('Payment::admin.payment.partial.newgateway',['gateway'=>$gateway]);
    }

    public function initial(Request $request){

        return view('Payment::admin.payment.initial');
    }

    public function edit(Request $request, $payment_id){

       $payment = $this->getRepo()->findWhere(['id'=>$payment_id])->first();
       return view('Payment::admin.payment.edit',['payment'=>$payment]);
    }

    public function store(Request $request){
        try {
            $datas = $request->except(['_token']);
            $datas['live_credentials'] = $datas['mode'] == "live"?json_encode($datas['credentials']):"{}";
            $datas['sandbox_credentials'] = $datas['mode'] == "sandbox"?json_encode($datas['credentials']):"{}";
            unset($datas['credentials']);
           $payment = $this->getRepo()->create($datas);
           
            return response()->json(
                [
                    'message'  => trans('messages.success.updated', ['Module' => "Payment: ".$payment->name]),
                    'code'     => 204,
                    'redirect' => trans_url('/admin/shop/payment/'.$payment->id.'/edit')
                ],
                201);
            
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ],
                400);
        }
    }
}