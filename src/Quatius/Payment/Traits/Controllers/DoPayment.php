<?php
namespace Quatius\Payment\Traits\Controllers;

use Illuminate\Http\Request;
use Quatius\Payment\Models\PaymentMethod;
use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;
use Event;

use Omnipay\Common\ItemBag;

trait DoPayment{

    private $order = null;
    private $payment = null;
    private $gateway = null;
    private $params = [];
    private $orderData = [];

    public function paymentRoute(Request $request, $gatewayName, $routeName="notification", $orderNumber=""){
        
        if (!config('quatius.payment.'.$gatewayName.'.enable',true)){
            return;
        }
        
        $routeExecute = config('quatius.payment.'.$gatewayName.'.routes.'.$routeName,null);
        $redirect = "";
        if (is_callable($routeExecute))
        {
            return $routeExecute($request);
        }
        elseif (is_string($routeExecute)){
            eval($routeExecute);
        }
        
        return $redirect;
    }
    
    private function redirectToPaymentGateway(Request $request, $response){

        if ($response->getRedirectMethod() == 'GET'){
            return redirect($response->getRedirectUrl());
        }
        else{
            $this->payment = $this->order->getPaymentMethod();

            $this->order->setPaymentProcessingForm(view($this->order->getPaymentMethod()->getProccesingForm(), ['response'=>$response,'payment'=>$this->payment,'order'=>$this->order])->render());

            $this->order->setPaymentData('redirect-response', $response->getData()); // also save session.

            return redirect($request->get('url-processing', $this->order? $this->order->onRedirectPayment('processing') : url('/')));   
        }
    }

    private function evaluteRespose(Request $request, $response, $redirect=true){
        if ($response)
        {
            ///debug only   
            if (config('app.debug', true) && config('quatius.payment.debug', false))
                return $this->gatewayResponseTest($response);
            
            if ($response->isSuccessful()){
                $this->showMessage("message", trans('Payment::payment.payment-completed'));

                $request->request->set('url-completed', $this->order->onRedirectPayment('completed'));

                $receiptNo = isset($response->getData()[$this->payment->getConfig('confirm.receipt_field','')])?$response->getData()[$this->payment->getConfig('confirm.receipt_field','')]:'';
                $this->confirmPayment($request, $response, $receiptNo);
                if ($redirect){
                    
                    return $this->returnToCompleted($request);
                }
                
                return true;
            }
            elseif ($response->isRedirect()){
                if (!$redirect)
                    return false;

                return $this->redirectToPaymentGateway($request, $response);
            }
        }

        $this->failedPayment($request, $response);

        if ($redirect){
            $msg = null;
            if ($response){
                $msg = $response->getMessage();
            }
            $msg = $request->get('error-msg', $msg);
            
            
            $this->showMessage("warning",$msg?:trans('Payment::payment.payment-failed'));
            
            return $this->returnToCheckout($request);
        }
        return false;
    }
    
    public function returnToCart(Request $request){
        if (!$this->order) $this->initialise($request, false);

        return redirect($request->get('url-cart', $this->order? $this->order->onRedirectPayment('cart') :url('/')));        
    }
    
    private function returnToCheckout(Request $request){
        if (!$this->order) $this->initialise($request, false);
        
        return redirect($request->get('url-checkout', $this->order? $this->order->onRedirectPayment('checkout') :url('/')));
    }
    
    public function returnToCompleted(Request $request){
        if (!$this->order) $this->initialise($request, false);
        
        return redirect($request->get('url-completed', $this->order? $this->order->onRedirectPayment('completed') :url('/')));
    }
    
    private function initialise(Request $request, $withError = true){
        try{
            $this->order = getPaymentOrder();
        
        }catch (\Exception $e){
            $this->showMessage("danger",$e->getMessage());
            return false;
        }
        
        if (!$this->order){
            if ($withError) $this->showMessage("warning", trans('Payment::payment.payment-session-expired'));
            return false;
        }
        
        $this->payment = $this->order->getPaymentMethod();

        if (!$this->payment){
            if ($withError) 
                $this->showMessage("danger", trans('Payment::payment.payment-method-not-found'));
            return false;
        }

        $this->gateway = $this->getGateway($this->payment);

        $this->params = $this->getParams($request);
        return true;
    }

    public function gatewayCheckout(Request $request){

        if (!$this->initialise($request))
            return $this->returnToCart($request);

        $this->pendingCheckoutPayment($request, null);

        $this->params["returnUrl"]=url("payment/checkout/complete");
        $this->params["cancelUrl"]=url("payment/checkout/cancel");

        $checkoutFunc = $this->payment->getConfig('checkout.submit', null);
        $response = null;
        try{
            if (is_string($checkoutFunc)){
                eval($checkoutFunc);
            }
            
            if ($response === null){
                $response = $this->gateway->purchase($this->params)->send();
            }
           
        }catch (\Exception $e){
            $this->showMessage("danger", $e->getMessage());
            app('log')->error(__METHOD__.' '.__LINE__. ' -'.$e->getMessage());
            
            return $this->returnToCart($request);
        }
        
        if ($response){
            if ($response->isRedirect()){
                return $this->redirectToPaymentGateway($request, $response);
            }
            else{
                if (!$response->isSuccessful())
                {
                    $this->showMessage("danger", $response->getMessage());
                    app('log')->error(__METHOD__.' '.__LINE__. ' -'.$response->getMessage());
                }
                // TODO: successful may not need to handle yet
            }
        }
        return $this->returnToCart($request);
    }

    public function gatewayProcessing(Request $request){

        if (!$this->initialise($request))
            return $this->returnToCart($request);

        return view('Payment::payment.processing', ['payment'=>$this->payment,'order'=>$this->order])->render();
    }
    
    public function gatewayCheckoutComplete(Request $request){
        
        if (!$this->initialise($request))
            return $this->returnToCart($request);
            
        $response = null;
        
        $checkoutFunc = $this->payment->getConfig('checkout.complete', null);
        try {
            if ($checkoutFunc && is_string($checkoutFunc)){
                eval($checkoutFunc);
            }

            if ($response === null){
                $response = $this->gateway->purchase($this->params)->send();
            }
        }catch (\Exception $e){
            $this->showMessage("danger", $e->getMessage());
            app('log')->error(__METHOD__.' '.__LINE__. ' -'.$e->getMessage());
            return $this->returnToCart($request);
        }
        
        if ($response && $response->isSuccessful()){
            $this->order->setPaymentData("checkout-requests", $request->all());
            $this->checkoutPayment($response, $response);
            return $this->returnToCheckout($request);
        }
        
        return $this->returnToCart($request);
    }
    
    public function gatewayCheckoutCancel(Request $request){
        return $this->returnToCart($request);
    }
    
    public function gatewayCheckoutConfirm(Request $request){

        if (!$this->initialise($request))
            return $this->returnToCart($request);

        unset($this->params['clientIp']);
        
        $this->params = array_merge($this->params, $this->order->getPaymentData("checkout-requests", []));

        $response = null;
        $confirmFunc = $this->payment->getConfig('checkout.confirm', null);
        try {
            if (is_string($confirmFunc)){
                eval($confirmFunc);
            }
        }
        catch (\Exception $e){
            $this->showMessage("danger", $e->getMessage());
            app('log')->error(__METHOD__.' '.__LINE__. ' -'.$e->getMessage());
            return $this->returnToCheckout($request);
        }
        
        if ($response == null){
            $this->order->resetPaymentMethod();
            
            $this->failedPayment($request, $response);
            
            $this->showMessage("warning",$request->get('error-msg')?:trans('Payment::payment.payment-failed'));
            
            return $this->returnToCheckout($request);
        }

        $this->pendingConfirmPayment($request, $response);

        if ($this->order->getPaymentData()){
            $this->checkoutPaymentConfirmed($request, $response);
        }
        
        return $this->evaluteRespose($request, $response, $this->order);
    }
    
    public function gatewayConfirm(Request $request){
        if (!$this->initialise($request)){
            return $this->order ? $this->returnToCheckout($request) : $this->returnToCart($request);
        }

        $response = null;
        
        try{
            $confirmFunc = $this->payment->getConfig('confirm.submit', null);
            
            if (is_string($confirmFunc)){
                eval($confirmFunc);
            }
            
            if ($response === null){
                $response = $this->gateway->purchase($this->params)->send();
            }

            $this->pendingConfirmPayment($request, $response);
            
        }catch (\Exception $e){
            $this->showMessage("danger", $e->getMessage());
            app('log')->error(__METHOD__.' '.__LINE__. ' -'.$e->getMessage());
            return $this->gatewayCancel($request);
        }
        return $this->evaluteRespose($request, $response);
    }

    public function gatewayCancel(Request $request){
        return $this->returnToCheckout($request);
    }
    
    public function gatewayConfirmComplete(Request $request){
        if (!$this->initialise($request)){
            return $this->order ? $this->returnToCheckout($request) : $this->returnToCart($request);
        }
        
        unset($this->params['clientIp']);
        
        $response = null;
        
        $confirmFunc = $this->payment->getConfig('confirm.complete', null);
        
        if (is_string($confirmFunc)){
            eval($confirmFunc);
        }

        if ($response === null){
            $response = $this->gateway->completePurchase($this->params)->send();
        }
        
        return $this->evaluteRespose($request, $response, $this->order);
    }
    
    protected function getGateway(PaymentMethod $payment){
        $gateway = Omnipay::create($payment->type);
        $settings= $payment->credentials;
        $settings['testMode'] = $payment->isDebugMode();

        $gateway->initialize($settings);
        
        if ($payment->isDebugMode()) {
            if (method_exists($gateway, 'setDeveloperMode')) {
                $gateway->setDeveloperMode(TRUE);
            } else {
                $gateway->setTestMode(TRUE);
            }
        }
        
        return $gateway;
    }

    protected function pendingConfirmPayment($request, $response=null){
        $this->notifyPayment($this->getOrderData('reference', ''), 'pending', $this->getOrderData('total',0), "", $response->getData());
        Event::fire('payment.confirm.pending',[$request, $response, $this->order]);
    }

    protected function pendingCheckoutPayment($request, $response){
        Event::fire('payment.checkout.pending',[$request, $response, $this->order]);
    }
    
    protected function checkoutPayment($request, $response){
        Event::fire('payment.checkout',[$this->order, $response]);
    }
    
    protected function checkoutPaymentConfirmed($request, $response){
        Event::fire('payment.checkout.confirmed',[$this->order, $response]);
        $this->notifyPayment($this->getOrderData('reference', ''), 'checkout', $this->getOrderData('total',0), "", $response->getData());
    
    }
    
    protected function failedPayment($request, $response){
        
        Event::fire('payment.failed',[$request, $this->order, $response]);
        $this->notifyPayment($this->getOrderData('reference', ''), 'decline', $this->getOrderData('total',0), "", $response->getData(), $response->getMessage());
    }

    protected function confirmPayment($request, $response, $tranId){
        Event::fire('payment.confirmed',[$this->order, $response, $tranId]);
        $this->notifyPayment($this->getOrderData('reference', ''), 'confirmed', $this->getOrderData('total',0), $tranId, $response->getData());
    }
    
    protected function notifyPayment($invoice_no, $status, $amt, $tranId, $data, $msg=""){
        Event::fire('payment.notify',[$invoice_no, $status, $amt, $tranId, $data, $msg]);
    }

    protected function notifyPaymentAddress($data, $response){
        Event::fire('payment.address.update',[$this->order, $data, $response]);
    }

    protected function getOrderData($keys, $def=null){
        if (!$this->orderData) return $def;

        return array_get($this->orderData, $keys, $def="");
    }

    public function showMessage($msgType = "info", $msg=""){
        flash()->message($msg, $msgType);
    }

    protected function getParams(Request $request){

        $this->payment = $this->order->getPaymentMethod();
        $this->orderData = $this->order->getPaymentDetails();

        $this->params = [
            "amount"=>sprintf("%.2f",$this->getOrderData('total',0)),
            "shippingAmount" => sprintf("%.2f", $this->getOrderData('shipping')),
            "taxAmount"=>sprintf("%.2f", $this->getOrderData('tax')),
            "currency"=>$this->getOrderData('currency', 'AUD'),
            "description"=>$this->getOrderData('description', ''),
            "transactionId"=>$this->getOrderData('reference', ''),
            "transactionReference"=>$this->getOrderData('reference', ''),
            "cardReference"=>'',
            "returnUrl"=>url("payment/confirm/complete"),
            "cancelUrl"=>url("payment/cancel"),
            "issuer"=>''
        ];
        
        $bags = new ItemBag();
        
        foreach ($this->getOrderData('items',[]) as $item){
            //['name'=>$itemLine->description, 'price'=>$itemLine->getPrice('final'), 'quantity'=>$itemLine->qty]
            $bags->add($item);
        }
        
        $this->params['items'] = $bags;
        
        $this->params = array_merge($this->params, $this->payment->getConfig('params', []));

        $this->params['clientIp'] = $request->ip();

        //     //$cards = json_decode('{"card-name":"John Doe","card-number":"2223016768739313","exp-month":"01","exp-year":"2021","cvc":"123","_token":"mFe0uCqL7eN15Dd9aOdGHT5gktMCZGoaQtJ19VcH","payment_id":"mEkR3zMw6L"}',true);
        $cardDatas = $this->order->getPaymentBilling();

        if ( $request->session()->has('credit-card', null)){
            $cards = $request->session()->get('credit-card', null);
            
            // may need to store card form input// $request->session()->flash('credit-card', $cards);
            abort_if($this->payment->requiredForm() && $cards == null, 404, "Payment details required");
            
            if ($cards){
                $cardDatas["firstName"] = $cards['firstName'];
                $cardDatas["lastName"] = $cards['lastName'];
                $cardDatas["number"] = $cards['card-number'];
                $cardDatas["expiryMonth"] = $cards['exp-month'];
                $cardDatas["expiryYear"] = $cards['exp-year'];
                $cardDatas["cvv"] = $cards['cvc'];
            }
        }
        
        $this->params['card'] = new CreditCard($cardDatas);

        return $this->params;
    }
    
    public function gatewayNotificationTest(Request $request, $order_number){

        $posted = '{"mc_gross":"249.00","invoice":"OR53FAD388","protection_eligibility":"Ineligible","payer_id":"XTABZWDLXWQ9N","tax":"0.00","payment_date":"22:00:44 Dec 13, 2017 PST","payment_status":"Completed","charset":"windows-1252","first_name":"test","mc_fee":"6.28","notify_version":"3.8","custom":"","payer_status":"verified","business":"account@cinavision.com","quantity":"1","verify_sign":"ANQ-SoLP8ghQC5IsaxLVDxbpzxN6A8mp5qVW4i7u5O9Wvd0Gq-BYQ.82","payer_email":"buyer@cinavision.com","txn_id":"7XH17494X1041440H","payment_type":"instant","last_name":"user","receiver_email":"account@cinavision.com","payment_fee":"","receiver_id":"5X8TQS8WPVHLU","txn_type":"express_checkout","item_name":"","mc_currency":"AUD","item_number":"","residence_country":"AU","test_ipn":"1","handling_amount":"0.00","transaction_subject":"","payment_gross":"","shipping":"0.00","ipn_track_id":"79fb5cce4ee16"}';
        
        $request->request->add(json_decode($posted,true));
        return  $this->gatewayNotification($request, $order_number);
        
        
    }
    
    protected function gatewayResponseTest($response){
        
        $viewStr = "";
        
        if ($response->isSuccessful()){
            $viewStr.="Congratulations, your request was successful!";
        }
        elseif ($response->isRedirect()){
            if ($response->getRedirectMethod() == 'GET'){
                $viewStr.='<p><a href="'.$response->getRedirectUrl().'" class="btn btn-success">Redirect Now</a></p>';
            }else{
                
                $actionUrl = method_exists($response,'getRedirectUrl')?$response->getRedirectUrl():'';
                $actionData = method_exists($response,'getRedirectUrl')?$response->getRedirectData():[];
                if ($actionUrl){
                    $viewStr.='<form method="POST" action="'.$actionUrl.'">';
                    foreach($actionData as $key=>$value){
                        $viewStr.='<input type="hidden" name="'.$key.'" value="'.$value.'" />';
                    }
                    $viewStr .='<button class="btn btn-success">Redirect Now</button></form>';
                }else{
                    $viewStr .='<textarea id="temp_form">'.$response->getRedirectResponse()->getContent().'</textarea>';
                    $viewStr .='<button class="btn btn-success" onclick="$(\'body\').append($(\'#temp_form\').val());">Redirect Now</button></form>';
                }
            }
        }
        else{
            $viewStr.='Sorry, your request failed.';
        }
        
        
        $viewStr.='<p>The response object had the following to say:</p>
            
            
        <p><b>$response->getMessage()</b></p>
        <pre>{{ $response->getMessage() }}</pre>
            
        <p><b>$response->getCode()</b></p>
        <pre>{{ $response->getCode() }}</pre>
            
        <p><b>$response->getTransactionReference()</b></p>
        <pre>{{$response->getTransactionReference() }}</pre>
            
        <p><b>$response->getRequest()</b></p>
        <pre>{{ dump($response->getRequest()) }}</pre>
            
        <p><b>$response->getData()</b></p>
        <pre>{{ dump($response->getData()) }}</pre>
            
            
        ';
        
        
        return renderBlade($viewStr,['response'=>$response], 'blade');
    }
}