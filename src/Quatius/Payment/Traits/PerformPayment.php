<?php

namespace Quatius\Payment\Traits;

use Validator;

trait PerformPayment
{
    public $request_data = null;
    public $payment_process_form = "";
    private $_payment_method= null;

    public function setPaymentById($payment_id, $hashed = false){
        if ($hashed){
            $payment_id = intval(hashids_decode($payment_id));
        }
        
        $payment = $this->getPaymentMethods()->where('id',$payment_id)->first();
        
        $this->setPaymentMethod($payment);
    }

    public function getPaymentRepo(){
        return app('PaymentHandler');
    }

    public function getOrderSessionKey(){
        return 'payment_session';
    }

    public function updatePaymentKey(){
        if ($this->getOrderSessionKey() != getPaymentSessionKey())
            setPaymentSessionKey($this->getOrderSessionKey(), $this);
    }

    public function getPaymentMethods(){
        return $this->getPaymentRepo()->getPublished();
    }
    
    public function setPaymentMethod($paymentMethod){
        $this->_payment_method = $paymentMethod;
    }

    public function getPaymentMethod(){
        return $this->_payment_method;
    }

    public function getPaymentBilling(){
        if(!method_exists($this, 'getBillingDetail'))
            throw new \Exception("getPaymentBilling require override");

        if (!$this->getBillingDetail()) return [];

        return
        [
            'firstName'             =>$this->getBillingDetail()->first_name,
            "lastName"              =>$this->getBillingDetail()->last_name,

            'billingCompany'        =>$this->getBillingDetail()->company,
            "billingAddress1"       =>$this->getBillingDetail()->address_1,
            "billingAddress2"       =>$this->getBillingDetail()->address_2,
            "billingCity"           =>$this->getBillingDetail()->city,
            "billingPostcode"       =>$this->getBillingDetail()->postcode,
            "billingState"          =>$this->getBillingDetail()->state,
            "billingCountry"        =>$this->getBillingDetail()->country,
            "billingPhone"          =>$this->getBillingDetail()->phone_1,
            "email"                 =>$this->getBillingDetail()->email,

            'shippingFirstName'      =>$this->getShipmentDetail()->first_name,
            "shippingLastName"       =>$this->getShipmentDetail()->last_name,
            'shippingCompany'        =>$this->getShipmentDetail()->company,
            "shippingAddress1"       =>$this->getShipmentDetail()->address_1,
            "shippingAddress2"       =>$this->getShipmentDetail()->address_2,
            "shippingCity"           =>$this->getShipmentDetail()->city,
            "shippingPostcode"       =>$this->getShipmentDetail()->postcode,
            "shippingState"          =>$this->getShipmentDetail()->state,
            "shippingCountry"        =>$this->getShipmentDetail()->country,
            "shippingPhone"          =>$this->getShipmentDetail()->phone_1,
        ];
    }
    
    public function getPaymentDetails(){
        throw new \Exception("getPaymentDetails require override");
        return [
            "reference"=> $this->order_number,
            "description"=> '',
            "currency"=> 'AUD',
            "total"=> $this->total,
            "shipping"=> $this->shipping_cost,
            "tax"=> sprintf("%.2f",$this->total/11),
            "items"=>[
                //     [
                //         "name" => "",
                //         "quantity" => 0,
                //         "price" => "0.00"
                //     ]
                // 
            ]
        ];
    }

    public function getCheckoutButtons(){
        $types = $this->getPaymentMethods();

        $payCheckout = [];
        foreach ($types as $payMethod)
        {
            if ($payMethod->getConfig('express_checkout', false) && 
                $payMethod->getConfig('checkout', null))
            {
                $payCheckout[] = $payMethod;
            }
        }
        
        return collect($payCheckout);
    }

    public function getPaymentSelections($default=null){
        $types = $this->getPaymentMethods();
        
        if (!$default) $default = $this->getPaymentMethod();

        foreach ($types as $payMethod)
        {
            if (config('quatius.payment.payment_methods.'.$payMethod->type.'.confirm',null))
            {
                $logo = config('quatius.payment.payment_methods.'.$payMethod->type.'.confirm.logo','');
                if ($logo!=''){
                    $payMethod->logo= $logo;
                }
            }
            
            $payMethod->selected = ($default && $default->type == $payMethod->type);
        }
        
        return $types;
    }

    
    public function autoSelectPayment(){
        $payment = $this->getPaymentSelections(null)->first();
        if ($payment){
            $this->setPaymentMethod($payment);
        }
        return $this->_payment_method;
    }
    
    public function resetPaymentMethod()
    {
        $this->request_data = null;
        $this->payment_process_form = "";

        $this->_payment_method = null;
        savePaymentOrder($this);
    }
    
    public function isExpressCheckout($payment_method=null){
        if ($this->_payment_method == null){
            return false;
        }
        if ($payment_method && $payment_method->id != $this->_payment_method->id) return false;

        if($this->getPaymentData('checkout-requests') && config('quatius.payment.'.$this->_payment_method->type.'.checkout.complete', null) != null)
        {
            return true;
        }
        
        return false;
    }

    public function onRedirectPayment($to = 'cart'){
        
        if ($to == 'cart')
            return url('order/cart');

        if ($to == 'checkout')
            return url('order/checkout');

        if ($to == 'processing')
            return url('order/processing');

        if ($to == 'completed')
            return url('order/success/'.$this->order_number);
            
    }
    
    public function getPaymentProcessingForm(){
        return $this->payment_process_form;
    }

    public function setPaymentProcessingForm($postForm=""){
        $this->payment_process_form = $postForm;
    }
    
    public function setPaymentData($key = [], $value=null){
        
        if (!is_string($key)){
            $this->request_data = $key;
        }elseif (is_string($key)){
            if($this->request_data === null) $this->request_data = [];

            $this->request_data = array_set($this->request_data, $key, $value);
        }
        
        savePaymentOrder($this);
        
        session()->save();
    }
    
    public function getPaymentData($key=null, $def=null){
        if ($key===null) return isset($this->request_data)?$this->request_data:[];
        
        return array_get($this->request_data, $key, $def);
    }

    public function redirectPayment($request){
        $this->updatePaymentKey();
   
        if ($this->getPaymentMethod()){

            if ($request->input('checkout','0') == '1'){
                return redirect('payment/checkout/confirm');
            }

            if ($this->getPaymentMethod()->requiredForm())
            {
                $validator = Validator::make($request->all(), $this->getPaymentMethod()->getValidation());
                
                if ($validator->fails()) {
                    $this->throwValidationException($request, $validator);
                }
                
                $request->session()->flash('credit-card', $request->all());
            }
            
            return redirect('payment/confirm');
        }

        return $this->onRedirectPayment('cart');
    }
}
