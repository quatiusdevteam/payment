<?php 

return [

    'payment-method-not-found'=>'Please select a payment method!',
    'payment-completed'=>'Payment completed successfully',
    'payment-failed'=>'Unable to process your payment!',
    
    'payment-session-expired'=>'Your payment session expired!',
	
    'payment-method'=>'Payment Methods',
    'payment-gateway'=>'Select A Payment Method',
    'payment-name'=>'Name',
    'payment-type'=>'Type',
    'payment-mode' => 'Mode',
    'payment-live-credentials' => 'Live Credentials',
    'payment-sandbox-credentials' => 'Sandbox Credentials',
    'payment-params' => 'Params',
    'payment-notes' => 'Notes',
    'payment-status' => 'Status',
    'payment-name' => 'Payment Method',
    'payment-manage' => 'Manage Payment Method',
    
    'status' => 
		[
			'cancelled'=>'Cancelled',
			'pending' => 'Pending', 
			'confirmed'=>'Confirmed', 
			'accepted'=>'Accepted', 
			'refunded'=>'Refunded', 
			'refunded - partial' => 'Refunded Partially', 
      'completed'=>'Completed',
			'decline'=>'Decline',
			'unknown'=>'unknown'
		],
];
