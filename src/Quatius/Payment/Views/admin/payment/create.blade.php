<div class="box-header with-border">
    <h3 class="box-title"> New Payment </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#create-gateway'  data-load-to='#entry-user' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-user' data-href='{{Trans::to('admin/shop/payment/initial')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Profile</a></li>
            <!--<li><a href="#roles" data-toggle="tab">Details</a></li>-->
        </ul>

    @php 
        $gatewayValues = [""=>trans('Payment::payment.payment-gateway')];
        $gatewayValues['Supported']=[];
        $gatewayValues['Untested']=[];
        foreach ($gateways as $gateway ){
            if (config('quatius.payment.'.$gateway->getShortName(),null))
                $gatewayValues['Supported'][$gateway->getShortName()] = $gateway->getName();
            else
                $gatewayValues['Untested'][$gateway->getShortName()] = $gateway->getName();
        }
    @endphp
    {!!Form::horizontal_open()!!}
    {!!Form::select('payment_methods')
        ->label(trans('Payment::payment.payment-method')) 
        ->options($gatewayValues)
        ->setAttributes(['onchange'=> 'selectPayment(this)'])
    !!}

    {!!Form::text('new_methods')
    ->label("Or Enter Methods (Gateway Name)") 
    ->setAttributes(['onchange'=> 'selectPayment(this)'])
    !!}

    {!!Form::close()!!}
 
        <div id="create-gateway-entry">
        </div>
    
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
<script>
function selectPayment(itm){
    var selectedName = $(itm).val();
    if (selectedName == "") return;
   // $('#create-gateway-entry').load('{{URL::to('admin/shop/payment/create')}}' + '/' + selectedName);
    $.get('{{URL::to('admin/shop/payment/create')}}' + '/' + selectedName, function(result){
        $('#create-gateway-entry').html(result);
    }); 
}    

</script>
