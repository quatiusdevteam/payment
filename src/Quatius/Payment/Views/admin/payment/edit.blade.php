<div class="box-header with-border">
    <h3 class="box-title">Edit Payment </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-payment'  data-load-to='#entry-user' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-user' data-href='{{Trans::to('admin/shop/payment/initial')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
        {!!Form::horizontal_open()
        ->id('edit-payment')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(URL::to('admin/shop/payment/'. $payment->id))!!}

        @include('Payment::admin.payment.partial.entry')
        
        {!!Form::close()!!}
    
</div>
<div class="box-footer" >
    &nbsp;
</div>
