@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> {!! trans('Payment::payment.payment-name') !!} <small> {!! trans('Payment::payment.payment-manage') !!} </small>
@stop
@section('title')
{!! trans('Payment::payment.payment-name') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Payment::payment.payment-name') !!}</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
	@include("Payment::admin.payment.initial")
</div>
@stop
@section('tools')
<h4>

</h4>
@stop
@section('content')
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <th>{!! trans('Payment::payment.payment-name')!!}</th>
        <th>{!! trans('Payment::payment.payment-type')!!}</th>
        <th>{!! trans('Payment::payment.payment-mode')!!}</th>
        <th>{!! trans('Payment::payment.payment-notes')!!}</th>
        <th>{!! trans('Payment::payment.payment-status')!!}</th>
    </thead>
</table>
@stop
@section('script')
<style>
.show-on-unpublish .show-on-published{
    display:none;
}

.status-published .show-on-published{
    display:initial;
}

.status-published .show-on-unpublish{
    display:none;
}

.status-unpublish .show-on-published{
    display:none;
}

.status-unpublish .show-on-unpublish{
    display:initial;
}

.icon-unpublish:before{
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    transform: translate(0);
    font-size: 18px;
    content: "\f070";
    color:red;
}

.icon-published:before{
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    transform: translate(0);
    font-size: 18px;
    content: "\f06e";
    color:green;
}
</style>
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    
    oTable = $('#main-list').DataTable( {
        "ajax": '{{ URL::to('/admin/shop/payment') }}',
        "columns": [
        { "data": "name" },
        { "data": "type" },
        { "data": "mode" },
        { "data": "notes" },
        { "data": "status" }],
        "columnDefs": [
        	{
            	"render": function ( data, type, row ) {
						switch(parseInt(data)){
						case 1:
							return '<span class="hide">1</span><i class="icon-unpublish" aria-hidden="true"></i>';
						case 2:
							return '<span class="hide">2</span><i class="icon-published" aria-hidden="true"></i>';
						case 3:
							return '<span class="hide">3</span><i class="icon-configure" aria-hidden="true"></i>';
							
						}
                },"targets": 4
            },
        	{
        		"createdCell": function (td, cellData, rowData, row, col) {
        			$(td).addClass('text-center');
        		},"targets": [2,4]
        	},
		]
    }); 
    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('admin/shop/payment')}}' + '/' + d.id + '/edit');
    });

    
});
</script>
@stop
@section('style')
@stop


