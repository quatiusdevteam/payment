{!!Form::text('name')->value($payment->name)!!}
{!!Form::hidden('type')->value($payment->type)!!}
{!!Form::select('mode')->label('Mode ('.$payment->mode.')')
    ->options(['sandbox'=> 'sandbox', 'live'=> 'live', 'env'=> 'env'])-> value($payment->getAttributes()['mode'])
!!}
{!!Form::textarea('live_credentials')->value($payment->live_credentials)!!}
{!!Form::textarea('sandbox_credentials')->value($payment->sandbox_credentials)!!}
{!!Form::textarea('notes')->value($payment->notes)!!}
{!!Form::radios('status')->radios([
        ' <i class="icon-published" aria-hidden="true"></i> Active' => ['value' => 2],
        ' <i class="icon-unpublish" aria-hidden="true"></i> Disabled' => ['value' => 1]
    ])->check($payment->status)->inline()!!}
