
{!!Form::horizontal_open()
->id('create-gateway')
->method('POST')
->files('true')
->action(URL::to('admin/shop/payment'))!!}
    {!!Form::text('name')->value($gateway->getName())!!}
    {!!Form::hidden('type')->value($gateway->getShortName())!!}
    {!!Form::select('mode')
        ->options(['sandbox'=> 'sandbox', 'live'=> 'live'])
    !!}
    <br/>
    {!!Form::radios('status')->radios([
        ' <i class="icon-published" aria-hidden="true"></i> Active' => ['value' => 2],
        ' <i class="icon-unpublish" aria-hidden="true"></i> Disabled' => ['value' => 1]
    ])->check(1)->inline()!!}
    <hr/>
    @foreach ($gateway->getDefaultParameters() as $key=>$value)
    
        @php
        if ($key == 'testMode') continue;
        
        $intiValue = config('quatius.payment.'.$gateway->getShortName().'.initial.'.$key,'');

        $values = [];
        if (is_array($value)){
            $values = array_combine($value, $value);
        }

        if($intiValue){
            $value = $intiValue;
        }

        @endphp
        @if ($values && is_array($values))
            @if(is_array($value))
                {!!Form::multiselect('credentials['.$key.']')->options($values)->value($value)!!}
            @else
                {!!Form::select('credentials['.$key.']')->options($values)->value($value)!!}
            @endif
        @else 
            {!!Form::text('credentials['.$key.']')->value($value)!!}
        
        @endif
        
        
    @endforeach

    
{!! Form::close() !!}