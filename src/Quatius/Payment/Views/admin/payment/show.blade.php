<div class="box-header with-border">
    <h3 class="box-title"> View Payment Method  </h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-user' data-href='{{Trans::to('admin/shop/payment/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href="{{ trans_url('/admin/shop/payment/'.$payment->id) }}/edit"><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        
	        @if(user('admin.web')->isSuperUser())
	        	<button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-user' data-datatable='#main-list' data-href='{{ trans_url('/admin/shop/payment/'.$payment->id) }}' >
                    <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
                </button>
	        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >   
     @include('Payment::admin.payment.partial.entry')
</div>
<div class="box-footer" >
    &nbsp;
</div>
