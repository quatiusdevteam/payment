@php
    $payments = isset($payments)?$payments:(isset($order)?$order->getPaymentSelections():[])    
@endphp

@foreach($payments as $payMethod)
    <button type="submit" name='pay_checkout' value="{{$payMethod->getRouteKey()}}" onclick="jQuery(this.form).removeAttr('target');" class="btn btn-link" ><img style="height:42px" src="{!!$payMethod->getConfig('checkout.button', '')!!}" /></button>
@endforeach