@php
    $view =isset($view)?$view :'checkout-info';
    $payments = isset($payments)?$payments:(isset($order)?$order->getPaymentSelections():[])    
@endphp

@foreach($payments as $payment)
    @if($payment->getView($view, false))
        @include($payment->getView($view))
    @endif
@endforeach