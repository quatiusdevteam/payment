@include('Payment::payment.types.AfterPay.price-info')

@script
<script>
	$(document).ready(function(){
		$(document).on( "order-updating", function(event, response, itm ) {
			$('.afterpay-info').css("visibility","hidden");
		});
		
		$(document).on( "order-updated", function(event, response, itm ) {
			if(response.cart){
				var total = response.cart.total;
					
				total = parseFloat(total);
				var minPay = parseFloat($('.afterpay-failed').data('afterpay-min'));
				var maxPay = parseFloat($('.afterpay-failed').data('afterpay-max'));
				var payments = parseFloat($('.afterpay-failed').data('afterpay-payments'));
				
				if (total > minPay && total < maxPay){
					var divOf = Math.ceil(parseFloat(total)/parseInt(payments) * 100)/100;

					$('.afterpay-amount').text(divOf.toFixed(2));
					
					$('.afterpay-success').show();
					$('.afterpay-failed').hide();
				}
				else{
					$('.afterpay-success').hide();
					$('.afterpay-failed').show();
				}
				$('.afterpay-info').css("visibility","visible");
			}
		});
	});
</script>
@endscript