@php
	$isAmtDisallowed = floatVal($order->getPaymentDetails()["total"]) > floatVal($payment->getCredentialsAttribute()["amount-max"]) || floatVal($order->getPaymentDetails()["total"]) < floatVal($payment->getCredentialsAttribute()["amount-min"]);

@endphp

{!!
	Form::open()
	->action($isAmtDisallowed?"":url(config('quatius.shop.cart.url_prefix','order/').'confirm'))
	->method('POST')
!!}

{!!Form::hidden('payment_id')->forceValue($payment->getRouteKey())!!}

	@if ($payment->getLogo())
		<button class="btn-confirm btn btn-link" type="submit" {!!$isAmtDisallowed?'disabled':""!!} role="button" >
		<img style="height:60px"class="img-responsive" src="{{$payment->getLogo()}}"></button>
	@else
		<button class="btn-confirm btn btn-default btn-block" {!!$isAmtDisallowed?'disabled':""!!} type="submit" role="button" >{{$payment->name}}</button>
	@endif
	
{!! Form::close() !!}