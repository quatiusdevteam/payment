@php
    $price = isset($price)?$price:$order->getPaymentDetails()["total"];
    $afterpaySetting = $payment->getCredentialsAttribute();

    $numOfPayments = isset($afterpaySetting["num_of_payments"])?$afterpaySetting["num_of_payments"]:"4";
    $numOfPayments = $numOfPayments?:1; // divide by zero;

    $ofFour = number_format(ceil((floatVal("$price")/$numOfPayments)*100)/100, 2);

	$afterpayMin = isset($afterpaySetting["amount-min"])?$afterpaySetting["amount-min"]:"0";
	$afterpayMax = isset($afterpaySetting["amount-max"])?$afterpaySetting["amount-max"]:"0";
	$isAmtDisallowed = !(floatVal($price) > floatVal($afterpayMin) && floatVal($price) < floatVal($afterpayMax));
    $messageDisallowed = isset($afterpaySetting["limit_msg"])?$afterpaySetting["limit_msg"]:"available on orders";
    $messagePayments = isset($afterpaySetting["payment_msg"])?$afterpaySetting["payment_msg"]:$numOfPayments.' x fortnightly payments of ';
    
@endphp
<div class="afterpay-info">
    <div class="afterpay-failed"{!!$isAmtDisallowed?'':' style="display:none"'!!} data-afterpay-min="{{$afterpayMin}}" data-afterpay-max="{{$afterpayMax}}" data-afterpay-payments="{{$numOfPayments}}">
        
        <span class="afterpay-logo-bg"> Afterpay </span> {!!$messageDisallowed!!} ${{$afterpayMin}} to ${{$afterpayMax}}. <a type="button" style="cursor:pointer" data-toggle="modal" data-target="#afterpay-info">
            Learn More
        </a>
    </div>
    <div class="afterpay-success" d {!!$isAmtDisallowed?' style="display:none"':''!!}>
        <span>{!!$messagePayments!!} $<span class="afterpay-amount">{{$ofFour}}</span> with
        <span class="afterpay-logo-bg"> Afterpay </span>
            <a type="button" style="cursor:pointer;white-space: nowrap;" data-toggle="modal" data-target="#afterpay-info">
                Learn More
            </a>
        </span>
    </div>
</div>

@script

<div class="modal fade" id="afterpay-info" tabindex="-1" role="dialog" aria-labelledby="afterpay-info-label" style="display: none;">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" style="margin-bottom: -25px;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        
        <img class="hidden-xs" style="width: 100%" src="{{asset('/images/shop/payment_logo/after-desktop-banner.png')}}">
        <img class="visible-xs" style="width: 100%" src="{{asset('/images/shop/payment_logo/after-mobile-banner.png')}}">
        </div>

        <div class="modal-footer" style="text-align:center">
                <a class="terms" href="https://www.afterpay.com/en-AU/terms" target="_blank">
                    <span class=""></span> Read the full terms &amp; conditions at Afterpay
                </a>
            </div>
        
    </div>
    </div>
</div>

<style>
    .afterpay-info .afterpay-logo-bg{
        overflow: hidden;
        text-indent: -10000px;
        background-size: contain;
        display: inline-block;
        display: -webkit-inline-box;
        position: relative;
        top: .7rem;
        padding: 0px 10px;
        text-align: initial;
        background: url({{asset('/images/shop/payment_logo/afterpay-logo.svg')}}) center center no-repeat;      
    }
</style>

@endscript