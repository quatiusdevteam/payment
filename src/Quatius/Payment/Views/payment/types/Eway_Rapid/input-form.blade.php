@includeScript('https://secure.ewaypayments.com/scripts/eCrypt.js')

<?php 

if ($payment->isDebugMode())
{
    $creditcard = array(
        "EWAY_CARDNAME" => "Test",
        "EWAY_CARDNUMBER" => "4444333322221111",
        "EWAY_CARDEXPMONTH" => "11",
        "EWAY_CARDEXPYEAR" => "20",
        "EWAY_CARDCVN" => "234",
    );
}
else
{
    $creditcard = array(
        "EWAY_CARDNAME" => "",
        "EWAY_CARDNUMBER" => "",
        "EWAY_CARDEXPMONTH" => "",
        "EWAY_CARDEXPYEAR" => "",
        "EWAY_CARDCVN" => "",
    );
}

$months = [
    '' => 'Month',
    '01' => '01',
    '02' => '02',
    '03' => '03',
    '04' => '04',
    '05' => '05',
    '06' => '06',
    '07' => '07',
    '08' => '08',
    '09' => '09',
    '10' => '10',
    '11' => '11',
    '12' => '12'
];

$years = array(''=>'Year');
$start = date ('Y');
$end = $start + 7;

for ($i = $start; $i <= $end; $i++) {
    $years[$i] =  $i - 2000;
}

?>

@php
Form::framework('Nude');
@endphp

{!! Form::open()
	->action(url(config('quatius.shop.cart.url_prefix','order/').'confirm'))
	->method('POST')
	->setAttributes([
		"id" => "eway_form",
		"name" => "creditCardForm",
		'onsubmit' => 'return ewayEncryptField()', 
		'data-eway-encrypt-key' => $payment->credentials['encryption-key']
	])

!!}

<div class="row" id="eway_form" style="margin: auto;">
	<div class="col-sm-8">
		<div class="form-group" style="padding-top:3px; padding-bottom:3px;margin-top:8px">
			<label for="eway_rapid-cc-ownerf">Card Holder Name </label>
			<div class="input-wrapper">
				{!!Form::text('EWAY_CARDNAME','')
                	->value($creditcard["EWAY_CARDNAME"])
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-name",
                		"id"=>"eway_rapid-cc-ownerf",
                		"required"=>"required"
                	])
                !!}
			</div>
		</div>
		<div class="form-group" style="padding-top:3px; padding-bottom:3px;">
			<label for="eway_rapid-cc-number" class="inputLabelPayment">Credit Card Number </label>
			<div class="input-wrapper">
				{!!Form::text('EWAY_CARDNUMBER','')
                	->value($creditcard["EWAY_CARDNUMBER"])
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-number",
                		"data-eway-encrypt-name"=>"EWAY_CARDNUMBER",
                		"id"=>"eway_rapid-cc-number",
                		"required"=>"required"
                	])
                !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-6 col-md-6" style="padding:0px;">
				<label for="eway_rapid-cc-expires-month" style="display:block;">Expiry Date </label>
				<div class="col-sm-6 col-md-6" style="padding:3px 0px;">
				
				{!!Form::select('EWAY_CARDEXPMONTH')->options($months)
                	->select($creditcard["EWAY_CARDEXPMONTH"])
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-exp-month",
                		"required"=>"required"
                	])
				!!}
				</div>
				<div class="col-sm-6 col-md-6" style="padding:3px 0px;">
                {!!Form::select('EWAY_CARDEXPYEAR','')->options($years)
                	->select($creditcard["EWAY_CARDEXPYEAR"])
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-exp-year",
                		"required"=>"required"
                	])
                !!}

				</div>
			</div>
			<div class="col-sm-6 col-md-6" style="padding:3px 0px;">
				<label for="eway_rapid-cc-cvv" style="display:block;">CVN </label>
				<div class="input-group">
				<div class="input-wrapper">
				{!!Form::text('EWAY_CARDCVN','')
                	->value($creditcard["EWAY_CARDCVN"])
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-csc",
                		"data-eway-encrypt-name"=>"EWAY_CARDCVN",
                		"required"=>"required"
                	])
                !!}
				</div>
					<a tabindex="0" class="input-group-addon" role="button" data-placement="left" data-toggle="popover" data-trigger="focus" title="" data-content="Locate CVN number at the back of the credit card." data-original-title="Information"><span class="glyphicon glyphicon-question-sign"></span></a>
				</div>
			</div>
		</div>
		<button class="btn-confirm btn btn-default btn-block" type="submit" role="button" ><i class="fa fa-credit-card" aria-hidden="true"></i> PROCESS PAYMENT</button>
	
	</div>
	<div class="col-sm-4">
		<br><br>
<!-- Begin eWAY Linking Code -->
<div id="eWAYBlock">
    <div style="text-align:center;">
        <a href="https://www.eway.com.au/secure-site-seal?i=11&amp;s=3&amp;pid=53879195-9fa0-4b73-ab70-385a374fd32c&amp;theme=0" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
            <img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&amp;size=3&amp;pid=53879195-9fa0-4b73-ab70-385a374fd32c&amp;theme=0">
        </a>
    </div>
</div>
<!-- End eWAY Linking Code -->
		
	</div>
</div>

{!!Form::close()!!}

@php
Form::framework('TwitterBootstrap3');
@endphp



@script
<script type="text/javascript">

function ewayEncryptField(){
	return;
	jQuery("#eway_form input[data-eway-encrypt-name]").removeAttr('name');
	
	jQuery("#eway_form input[data-eway-encrypt-name]").each(function(idx)
	{
		var userInput = jQuery(this);

		var nameAttr = userInput.attr('data-eway-encrypt-name');

		if (jQuery('#eway_form input[name="'+nameAttr+'"]').length == 0)
			userInput.after('<input type="hidden" name="'+nameAttr+'" />');

		jQuery('#eway_form input[name="'+nameAttr+'"]').val(eCrypt.encryptValue(userInput.val()));
	});
	jQuery('#eway_loader').modal('show');
}

</script>
@endscript
