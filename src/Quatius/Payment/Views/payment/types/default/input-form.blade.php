<?php 

$months = [
    '' => 'Month',
    '01' => '01',
    '02' => '02',
    '03' => '03',
    '04' => '04',
    '05' => '05',
    '06' => '06',
    '07' => '07',
    '08' => '08',
    '09' => '09',
    '10' => '10',
    '11' => '11',
    '12' => '12'
];

$years = array(''=>'Year');
$start = date ('Y');
$end = $start + 7;

for ($i = $start; $i <= $end; $i++) {
    $years[$i] =  $i;
}
?>

@php
Form::framework('Nude');
@endphp

{!! Form::open()
	->action(url(config('quatius.shop.cart.url_prefix','order/').'confirm'))
	->method('POST')
	->setAttributes([
		"name" => "creditCardForm",
	])->rules($payment->getConfig('input-form.validation',[]))

!!}
{!!Form::hidden('payment_id')->forceValue($payment->getRouteKey())!!}
<div class="row" style="margin: auto;">
	<div class="col-sm-6">
		<div class="form-group" style="padding-top:3px; padding-bottom:3px;margin-top:8px">
			<label for="frmFirstNameCC" style="display:block;">Card Holder Name </label>
			<div class="col-sm-6" style="padding:3px 0px;">
				{!!Form::text('firstName','')
					->placeholder('First Name')
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-first-name",
                		"id"=>"frmFirstNameCC",
                		"required"=>"required"
                	])
                !!}
			</div>
			<div class="col-sm-6" style="padding:3px 0px;">
				{!!Form::text('lastName','')
					->placeholder('last Name')
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-last-name",
                		"id"=>"frmLastNameCC",
                		"required"=>"required"
                	])
                !!}
			</div>
		</div>
		<div class="form-group" style="padding-top:3px; padding-bottom:3px;">
			<label for="frmCCCVC" class="inputLabelPayment">Credit Card Number </label>
			<div class="input-wrapper">
				{!!Form::text('card-number','')
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-number",
                		"id"=>"frmCCNum",
                		"required"=>"required"
                	])->value($payment->isDebugMode()?'2223016768739313':'')
                !!}
			</div>
		</div>
		
	</div>
	<div class="col-sm-6">
		<img class="img-responsive" src="{{$payment->getLogo()}}">
	</div>
	<div class="col-sm-12">
		<div class="form-group">
			<div class="col-sm-6 col-md-6" style="padding:0px;">
				<label for="frmCCExpMM" style="display:block;">Expiry Date </label>
				<div class="col-sm-6 col-md-6" style="padding:3px 0px;">
				
				{!!Form::select('exp-month','')->options($months)
                	->setAttributes([
                		"class" => "form-control",
                		"id"=>"frmCCExpMM",
                		"autocomplete" => "cc-exp-month",
                		"required"=>"required"
                	])
				!!}
				</div>
				<div class="col-sm-6 col-md-6" style="padding:3px 0px;">
                {!!Form::select('exp-year','')->options($years)
                	->setAttributes([
                		"class" => "form-control",
                		"id"=>"frmCCExpMM",
                		"autocomplete" => "cc-exp-year",
                		"required"=>"required"
                	])
                !!}

				</div>
			</div>
			<div class="col-sm-6 col-md-6" style="padding:3px 0px;">
				<label for="frmCCCVC" style="display:block;">CVC </label>
				<div class="input-group">
				<div class="input-wrapper">
				{!!Form::text('cvc','')
                	->setAttributes([
                		"class" => "form-control",
                		"autocomplete" => "cc-csc",
                		"id"=>"frmCCCVC",
                		"required"=>"required"
                	])
                !!}
				</div>
					<a tabindex="0" class="input-group-addon" role="button" data-placement="left" data-toggle="popover" data-trigger="focus" title="" data-content="Locate CVC number at the back of the credit card." data-original-title="Information"><span class="glyphicon glyphicon-question-sign"></span></a>
				</div>
			</div>
		</div>
	</div>
</div>
<button class="btn-confirm btn btn-default btn-block" type="submit" role="button" ><i class="fa fa-credit-card" aria-hidden="true"></i> PROCESS PAYMENT</button>
	
{!!Form::close()!!}

@php
Form::framework('TwitterBootstrap3');
@endphp

@script
<script type="text/javascript">
	$(document).ready(function(){
		jQuery('[data-toggle="popover"]').popover();
	});
</script>
@endscript
