{!!
	Form::open()
	->action(url(config('quatius.shop.cart.url_prefix','order/').'confirm'))
	->method('POST')
!!}

{!!Form::hidden('payment_id')->forceValue($payment->getRouteKey())!!}

@if($order->isExpressCheckout($payment))
	{!!Form::hidden('checkout')->value(1)!!}
@endif
@if ($payment->getLogo())
	<button class="btn-confirm btn btn-link" type="submit" role="button" >
	<img style="height:60px"class="img-responsive" src="{{$payment->getLogo()}}"></button>
@else
	<button class="btn-confirm btn btn-default btn-block" type="submit" role="button" >{{$payment->name}}</button>
@endif

{!! Form::close() !!}