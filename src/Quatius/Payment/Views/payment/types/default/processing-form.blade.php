@script
	<script type="text/javascript">

	jQuery(document).ready(function(){	
		jQuery('#std-default-form').submit();
	});
	</script> 
@endscript

<form id="std-default-form" action="{!!$response->getRedirectUrl()!!}" method="post">
@if ($response->getRedirectData())
    @foreach($response->getRedirectData() as $key=>$value)
    
    <input type="hidden" name="{!!$key!!}" value="{!!$value!!}" />
    
    @endforeach
@endif
</form>