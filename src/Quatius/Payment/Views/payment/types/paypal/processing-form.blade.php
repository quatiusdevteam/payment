@script
	<script type="text/javascript">

	jQuery(document).ready(function(){	
		jQuery('#paypal_form').submit();
	});
	</script> 
@endscript

<form id="paypal_form" action="https://www.{{config('services.paypal.mode')==''?'':config('services.paypal.mode').'.'}}paypal.com/cgi-bin/webscr" method="post">
  <input type="hidden" name="notify_url" value="{{url('/')}}/payment/paypal/notification">
  <input type="hidden" name="return" value="{{url('/')}}/payment/paypal/return/{{$order->order_number}}">
  <input type="hidden" name="cancel_return" value="{{url('/')}}/payment/paypal/cancel">
 <input type="hidden" name="shopping_url" value="{{url('/')}}">
    
<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">

<input type="hidden" name="business" value="{{config('services.paypal.email')}}">
	
<input type="hidden" name="invoice" value="{{$order->order_number}}">
<?php $i=1; ?>
@foreach($order->getProducts() as $item)

	<input type="hidden" name="item_name_{{$i}}" value="{{$item->description}}">
	<input type="hidden" name="amount_{{$i}}" value="{{$item->getPrice('final',true)}}">
	<input type="hidden" name="quantity_{{$i}}" value="{{$item->qty}}">
	<?php $i++; ?>
@endforeach

	<input type="hidden" name="handling_cart" value="{{$order->shipping_cost}}">
	@if ($order->discount > 0)
	<input type="hidden" name="discount_amount_cart" value="{{$order->discount}}">
	@endif
  <input type="hidden" name="no_note" value="1">
  <input type="hidden" name="currency_code" value="AUD">

  <!-- Enable override of buyers's address stored with PayPal . -->
  <input type="hidden" name="address_override" value="1">
  <!-- Set variables that override the address stored with PayPal. -->
  <input type="hidden" name="first_name" value="{{$order->getShipmentDetail()->first_name}}">
  <input type="hidden" name="last_name" value="{{$order->getShipmentDetail()->last_name}}">
  <input type="hidden" name="address1" value="{{$order->getShipmentDetail()->address_1}}">
  <input type="hidden" name="city" value="{{$order->getShipmentDetail()->city}}">
  <input type="hidden" name="state" value="{{$order->getShipmentDetail()->state}}">
  <input type="hidden" name="zip" value="{{$order->getShipmentDetail()->postcode}}">
  <input type="hidden" name="country" value="AU">
<!-- <input type="image" name="submit"
    src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
    alt="PayPal - The safer, easier way to pay online"> -->
</form>