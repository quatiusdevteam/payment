
<div class="row payment-section-area">
	<div class="col-xs-12">
			@php
				$selectedPayment = $payments->first();
			@endphp

				@if ($payments->count() > 1)
						<h2>Please select a payment method</h2>
						<hr>
        @endif
        
		@foreach($payments as $payment)
		
			@php
				if ($payment->selected) $selectedPayment = $payment;
			@endphp
			<div class="payment {{$payment->selected?'active':''}}" data-id="{{$payment->getRouteKey()}}" data-name="{{$payment->name}}" >
				<h4>Pay with {{$payment->name}}</h4>
				@include($payment->getOrderForm(), ['order'=>$order, 'payment'=>$payment])
			</div>
					
        	@endforeach
        
    </div>
</div>