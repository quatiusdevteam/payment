<?php
return[
    /*
    AfterPay : "mediabeastnz/omnipay-afterpay": "~2.1"
    */
    'debug'=>env('SHOP_PAYMENT_DEBUG', false),
    'extra-gateways'=>[
        'PayPal_Express', 'PayPal_Rest'
    ],
    
    'AfterPay' => [
        'name' => 'AfterPay',
        'initial'=>[
            "merchantId"=>"10",
            "merchantSecret"=>"f4b17f6318f90d7e63c0442875512a4eefdb0007c2f26da14cc0902f52e23b80118bc476757af7a9a3666ddccd6d87254538da6d3f6dee1d947a71e96a43046b"
        ],
        'views'=>[
            'processing-form' => 'Payment::payment.types.AfterPay.processing-form',
            'product-price' => 'Payment::payment.types.AfterPay.product-price',
            'checkout-info' => 'Payment::payment.types.AfterPay.checkout-info',
            'confirm-info' => 'Payment::payment.types.AfterPay.confirm-info'
        ],
        "confirm"=>[
            "receipt_field" => "id",
            "logo" => asset("images\shop\payment_logo\afterpay-shop-now.png"),
            "submit" => '
                $afterpayMin = isset($payment->credentials["amount-min"])?$this->payment->credentials["amount-min"]:"0";
	            $afterpayMax = isset($this->payment->credentials["amount-max"])?$this->payment->credentials["amount-max"]:"1000";
                
                if (!(floatVal($this->getOrderData("total",0)) > floatVal($afterpayMin) && floatVal($this->getOrderData("total",0)) < floatVal($afterpayMax))){
                    $messageDisallowed = isset($this->payment->credentials["limit_msg"])?$this->payment->credentials["limit_msg"]:"available on orders";
                
                    throw new \Exception("Afterpay $messageDisallowed \$$afterpayMin to \$$afterpayMax");
                }
            '
            
        ],
    ],
    'PayPal_Rest' => [
        'name' => 'PayPal Rest',
        'initial'=>[
            'clientId' => 'Ab50iyVeWAF0gmZQZT553v8BNKzxX-JdcvKOX3HKQ8PbHjp5F_mUBFU-tdDq130vzWMVoxJuWJoDcA3c',
        ///'APP-80W284485P519543T',
            'secret'   => 'EDtv84hNy_oGp26jgJ1oNSXHdJeRhw6gPimlFt95LmqIi9U6zomjIVZPhHJZgCxK5qqzWdPL-olUnfuZ'
        ],
        'input-form'=>[
            'validation'=>[
                    'firstName' => 'required|max:255',
                    'lastName' => 'required|max:255',
                    'card-number' => 'required|min:16',
                    'exp-month'=>'required',
                    'exp-year'=>'required',
                    'cvc' => 'required|min:3',
                ]
        ],
    ],
    'PayPal_Express' => [
        'name' => 'PayPal',
        'express_checkout'=>true,
        'status_map'=>[
			'Refunded'=>'refunded', 
			'Partially Refunded'=>'refunded - partial', 
            "Completed" => 'completed'
        ],
        'initial'=>[
            'username'=>'account_api1.cinavision.com',
            'password'=>'1374811947',
            'signature'=>'AFcWxV21C7fd0v3bYYYRCpSSRl31Alxf-K1sReT.eLhHd599yYdK1By-',
            'solutionType'=>'Sole',
            'landingPage'=>['Login','Billing'],
            "brandName"=>config('app.name',''),
            "logoImageUrl"=>url('images/logo.png'),
            "borderColor"=>"ffffff"
        ],
        'checkout'=>[
            "button" => 'https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif',
            "complete" => '
                $checkout=$this->gateway->fetchCheckout($this->params);
                $checkout->setToken($request->get("token",""));
                $response = $checkout->send();
                if ($response->isSuccessful()){
                    $result =  $response->getData();
                    
                    if (isset($result["SHIPTONAME"])){
                        $name = explode(" ", $result["SHIPTONAME"]);
                        $data =[
                            "first_name"           => $name[0],
                            "last_name"            => isset($name[1])?$name[1]:"",
                            "email"                => $result["EMAIL"],
                            "address_1"			   => $result["SHIPTOSTREET"],
                            "city"				   => $result["SHIPTOCITY"],
                            "state"				   => isset($result["SHIPTOSTATE"])?$result["SHIPTOSTATE"]:null,
                            "postcode" 			   => $result["SHIPTOZIP"],
                            "country"			   => $result["SHIPTOCOUNTRYCODE"],
                        ];
                        
                        if (isset($result["SHIPTOPHONENUM"])){
                            $data["phone_1"] = $result["SHIPTOPHONENUM"];
                        }

                        if (isset($result["PHONENUM"])){
                            $data["phone_1"] = $result["PHONENUM"];
                        }
                        
                        if (isset($result["SHIPTOSTREET2"])){
                            $data["address_2"] = $result["SHIPTOSTREET2"];
                        }

                        
                        $this->notifyPaymentAddress($data, $response);
                    }
                }'
            ,
            "confirm" => '
                $response = $this->gateway->completePurchase($this->params)->send();
            '
        ],
        "confirm"=>[
            "logo" => 'https://www.paypalobjects.com/webstatic/en_AU/i/buttons/btn_paywith_primary_l.png',
            "receipt_field" => "PAYMENTINFO_0_TRANSACTIONID",
            "submit" => '
                $this->params["noShipping"] = "true";'
            ,
        ],
        "mode" => env("PAYPAL_MODE", "sandbox"),
        "params"=>[
            "notifyUrl"=>url("payment/route/PayPal_Express/notification"),
        ],
        "routes"=>[
            "notification"=>'
                if ($request->isMethod("post")) {
                    $statMap = config("quatius.payment.PayPal_Express.status_map",[]);
                    $status = "unknown";
                    if (isset($statMap[$request->get("payment_status","")])){
                        $status = $statMap[$request->get("payment_status","")];
                    }
                    
                    $this->notifyPayment($request->get("invoice",""), $status, $request->get("mc_gross", 0), $request->get("txn_id",""),$request->all());
                }
            '
        ]
    ],
    'Eway_Rapid' => [
        'name' => 'Eway Rapid',
        'mode' => env('PAYPAL_MODE', 'sandbox'),
        'input-form'=>[''],
        'initial'=>[
            'apiKey' => 'C3AB9C6hf2jZzZJGuec/fGZc2Tooii9w8iz4GnpQrdlQzkJVbfooE67z+ekaApnGHfh2he',
            'password' => 'Q9xatIiU',
            'data-publicapikey'=>'epk-EC8A0841-6344-40D0-BCB3-56055A8FF3F6',
            'encryption-key'=>'rQJCB6Nxie5UQw0tY3icDmqQ/pckpHYXPBfTycpFdZfoQu4y7zzNXk7qZJTP/2vmmhLA4eYjo/9yyYC0kk60y74muoZ96JClu2KRTIERLwkKN4tzVcrkWIsugz7VrZIMqeIapOyG+faUafpfz9+qQFJ7vlrIAB7+bDSbMzJTIP/RkHSpMQShrS8+ysKe4pOe/mi6bJymG/1xuWXk9nRAcC93gUDE+65w1f7adEGC48VjB0X9HRhy7EzaogFoJMNNlqYVd830pDvMh+zRuk/RiVbRR52vyZn9XOoC013irVSBre03Wc+Bdg+/GVc7pYgDVuRiESA9IRABof1XNBHZ/Q=='
        ],
        'notes'=>[
            'sanbox'=>[
                'url'=>'https://sandbox.myeway.com.au/gbc/login.aspx',
                'login'=>'seyla.keth@quatius.com.au.sand',
                'password'=>'A1qwerty',
                'card'=>[
                    'master'=>[],
                    'visa'=>[]
                ]
            ]                
        ]
    ]
    
];