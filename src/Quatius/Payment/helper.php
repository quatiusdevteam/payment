<?php

/**
 *	Payment Helper  
 */

if (!function_exists('getPaymentOrder'))
{
    function getPaymentOrder()
    {
        $order = session(getPaymentSessionKey(), null);
        if ($order == null) throw new \Exception('No Payment order set');
        return $order;
    }
}

if (!function_exists('savePaymentOrder'))
{
    function savePaymentOrder($order)
    {
        session([getPaymentSessionKey()=> $order]);
    }
}

if (!function_exists('getPaymentSessionKey'))
{
    function getPaymentSessionKey()
    {
        return session('payment_order_session', '');
    }
}

if (!function_exists('setPaymentSessionKey'))
{
    function setPaymentSessionKey($sessionName, $order = null)
    {
        session(['payment_order_session'=>$sessionName]);
        if ($order)
            savePaymentOrder($order);
    }
}