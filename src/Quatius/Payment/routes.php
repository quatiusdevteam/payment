<?php

Route::group(['prefix'=>'payment/','middleware' => ['web'], 'namespace' => 'App\Modules\Payment\Controllers'], function() {
    
    Route::get('checkout', 'PaymentController@gatewayCheckout');
    Route::get('checkout/cancel', 'PaymentController@gatewayCheckoutCancel');
    Route::get('checkout/complete', 'PaymentController@gatewayCheckoutComplete');
    Route::get('checkout/confirm', 'PaymentController@gatewayCheckoutConfirm');
    Route::get('processing', 'PaymentController@gatewayProcessing');

    Route::get('confirm', 'PaymentController@gatewayConfirm');
    Route::get('confirm/complete', 'PaymentController@gatewayConfirmComplete');
    
    Route::get('cancel', 'PaymentController@gatewayCancel');
    Route::get('cart', 'PaymentController@returnToCart');
    Route::get('test', function (){
        return theme('b2b','default')->string('')->render();
    });
    
    if (config('app.debug')){
        Route::get('/payment/test/notification/{order_number?}', 'PaymentController@gatewayNotificationTest');
    }
});

Route::group(['prefix'=>'payment/','middleware' => ['api'], 'namespace' => 'App\Modules\Payment\Controllers'], function() {
    Route::post('route/{gateway}/{name}/{order_number?}', 'PaymentController@paymentRoute');
    Route::get('route/{gateway}/{name}/{order_number?}', 'PaymentController@paymentRoute');
});


Route::group(['prefix'=>'admin/shop',  'namespace' => 'App\Modules\Payment\Controllers\Admin'],function(){
    Route::group(['prefix'=>'payment'],function(){
        Route::get('/', 'PaymentController@index');
        Route::post('/', 'PaymentController@store');
        Route::get('/create', 'PaymentController@create');
        Route::get('/create/{gateway}', 'PaymentController@createGateway');
        Route::get('/initial', 'PaymentController@initial');
        Route::get('/{payment_id}/edit', 'PaymentController@edit');
        Route::put('/{payment_id}', 'PaymentController@update' );
    }); 
});